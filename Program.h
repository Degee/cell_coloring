/* 
 * File:   Program.h
 * Author: Degee
 *
 * Created on 22. říjen 2013, 18:27
 */

#include "Edge.h"
#include "Printer.h"
#include<string>
#include <sstream> 
#include <fstream>
#include<iostream>
#include<map>
#include<vector>
#include<limits.h>

using namespace std;

#ifndef PROGRAM_H
#define	PROGRAM_H

class Program {
public:
							Program(string fname = "data.txt");
							Program(const Program& orig);
	virtual					~Program();
	
	long int				bestScore;
	vector<unsigned char>	bestSolution;
	int						bestSolutionSize;
	vector<unsigned char>	stack;

	
	void					initGraph(string fname = "data.txt");
	void					run();
	void					print();
	unsigned char			***merge();
	
private:
	// No edge variable
	static const char		NO_EDGE = 255;
	// Edge without color
	static const char		NO_COLOR_EDGE = 0;
	// Highest color to be used
	static const unsigned char		MAX_VALUE = 15;
	
	/**
         * [*][ ][ ] - y <0,Ymax> position
         * [ ][*][ ] - x <0,Xmax> position
         * [ ][ ][*] - edge <0,1> : 0 - vertical; 1 - horizontal
         
	 */
	unsigned char			***GRAPH;
	int						row;
	int						col;
	
	vector<Edge*>			EDGE_ARRAY;
    map<int, int>			EDGE_INDEX;
	bool					validInsert(unsigned char edgeColor, 
										int vectorEdgeIndex);
	bool					checkEdgeColor(int x, int y, bool vertical, 
										unsigned char edgeColor);
	unsigned char			***merge(unsigned char * solution);

};

#endif	/* PROGRAM_H */

