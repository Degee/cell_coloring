/* 
 * File:   Edge.cpp
 * @Author: Jiří Levý, Lukáš Svoboda
 * 
 * @brief
 * Class represents Edge. It is used for purpose of indexing edges of
 * unknown color.
 * 
 * Created on 22. říjen 2013, 13:42
 */

#include "Edge.h"

Edge::Edge(int x, int y, bool vertical) {
	this->col = x;
	this->row = y;
	this->vertical = vertical;
}

Edge::Edge(const Edge& orig) {
}

Edge::~Edge() {
}

/*
string Edge::toString() {
	return new string("Edge [x=" . x . ", y=" . y . ", vertical=" + vertical
					. ", hashCode()=" . this->hashCode() . "]");
}
*/

int Edge::hashCode() {
	return this->createCode(this->col, this->row, this->vertical);
}

int Edge::createCode(int col, int row, bool vertical) {
	int hash = col * 10000;
	hash += row * 10;
	hash += vertical ? 1 : 9;
	
	return hash;
}

bool Edge::equals(Edge *obj) {
	if (obj == this)
			return true;
	return obj->vertical == this->vertical && obj->col == this->col
							&& obj->row == this->row;
}
