/* 
 * File:   Program.cpp
 * Author: Degee
 * 
 * Created on 22. říjen 2013, 18:27
 */

#include "Program.h"

Program::Program(string fname) {
	this->bestScore = LONG_MAX;
	
	this->initGraph(fname);
	
	for (int row = 0; row < this->row; row++) {
			for (int col = 0; col < this->col; col++) {
					unsigned char *node = GRAPH[row][col];

					// Vertical edge is unknown
					if (node[0] == this->NO_COLOR_EDGE) {
							Edge *e = new Edge(col, row, true);
							this->EDGE_ARRAY.push_back(e);
							this->EDGE_INDEX[e->hashCode()] = (int)this->EDGE_ARRAY.size()-1;
							
							/*Edge *tmp = this->EDGE_ARRAY[this->EDGE_ARRAY.size()-1];
							cout << tmp->vertical << endl;
							cout << tmp->x << endl;
							cout << tmp->y << endl;*/
					}

					// Horizontal edge is unknown
					if (node[1] == this->NO_COLOR_EDGE) {
							Edge *e = new Edge(col, row, false);
							this->EDGE_ARRAY.push_back(e);
							this->EDGE_INDEX[e->hashCode()] = (int)this->EDGE_ARRAY.size()-1;
							
							/*
							Edge *tmp = this->EDGE_ARRAY[this->EDGE_ARRAY.size()-1];
							cout << tmp->vertical << endl;
							cout << tmp->x << endl;
							cout << tmp->y << endl;
							 * */
					}
					//cout << "xxxxxxxxxxxxxxxxxxxxxxxx" << endl;
			}
	}
	
	cout << "Object constructed" << endl;
}

Program::Program(const Program& orig) {
}

Program::~Program() {
	
}

// Whole graph is composed of atomic values.
                // Atomic value can be graphicaly represented as 
                // a point with edge to the right and down:
                //
                //    o--
                //    |
                // 
                // o   is source node represented by position in array [x, y]
                // --  is horizontal edge represented by [y][x][1] byte value
                // |   is vertical edge represented by   [y][x][0] byte value
void Program::initGraph(string fname) {
	ifstream myfile(fname.c_str());
	int n,m,i = 0;	
	vector<vector<unsigned char*> > tmp; 
	vector <unsigned char*> in;
	string line;
	stringstream ss;
	
	i = 0;
	while(getline(myfile, line)){
		in.clear();
		ss.str("");
		ss.clear();
		ss << line;
		while (ss >> n >> m) {			
			unsigned char *push = new unsigned char[2];		
			push[0] = n;
			push[1] = m;	
			//cout << "pushing: " << n << " , " << m << endl;
			in.push_back(push);
		}
		tmp.push_back(in);
		//tmp[radek][sloupec][hodnota]
		i++;
	};
	this->row = i;
	this->col = in.size();
	
	this->GRAPH = new unsigned char**[this->row];
	for (int row = 0; row < this->row; row++) {
			this->GRAPH[row] = new unsigned char*[this->col];
			for (int col = 0; col < this->col; col++)
					this->GRAPH[row][col] = new unsigned char[2];
	}	
	for (int row = 0; row < this->row; row++) 
		for (int col = 0;col < this->col; col++) {
			this->GRAPH[row][col][0] = tmp[row][col][0];
			this->GRAPH[row][col][1] = tmp[row][col][1];
		}
	
	myfile.close();
	cout << "Init complete" << endl;
}

/**
	* Determine whether insertion of the given color into 
	* the given index is valid operation with respect to 
	* already inserted colors.
	* @param edgeColor Tested 'color' to be inserted
	* @param vectorEdgeIndex Index of 'edge'.
	* @param partialSolutionVector Array of 'colorized' edges.
	* @return
	*/
bool Program::validInsert(unsigned char edgeColor, int vectorEdgeIndex) {			
		Edge *edge = this->EDGE_ARRAY[vectorEdgeIndex];		
	
		if (edge->vertical) {
			return checkEdgeColor(edge->col+0, edge->row+0, false, edgeColor) &&
				checkEdgeColor(edge->col+0, edge->row+1, false, edgeColor) &&
				checkEdgeColor(edge->col-1, edge->row+0, false, edgeColor) &&
				checkEdgeColor(edge->col-1, edge->row+1, false, edgeColor) &&
				checkEdgeColor(edge->col-1, edge->row+0, true, edgeColor) && 
				checkEdgeColor(edge->col+1, edge->row+0, true, edgeColor);
		}
			return  checkEdgeColor(edge->col+0, edge->row+0, true, edgeColor) &&
					checkEdgeColor(edge->col+0, edge->row-1, true, edgeColor) &&
					checkEdgeColor(edge->col+1, edge->row-1, true, edgeColor) && 
					checkEdgeColor(edge->col+1, edge->row+0, true, edgeColor) && 
					checkEdgeColor(edge->col+0, edge->row+1, false, edgeColor) &&
					checkEdgeColor(edge->col+0, edge->row-1, false, edgeColor);
}

/**
	* Checks whether given edge[x,y,vertical] is of same color
	* as the given edgeColor argument. It it is then throws an
	* exception.
	*/
bool Program::checkEdgeColor(int col, int row, bool vertical, unsigned char edgeColor) {
		unsigned char * node;
		/*
		cout << "X: " <<x << endl;
		cout <<  "Y: "  <<y << endl;
		cout <<  "V: " << vertical << endl;
		cout <<"c:"  <<(int)edgeColor << endl;
		*/
		
		// Get node from graph
				
		if (col >= this->col || row >= this->row) {
			return true;
		}
		
		if (col < 0 || row < 0) {
			return true;
		}
		
		node = this->GRAPH[row][col];
		
		// Get node's edge
		unsigned char edge = node[vertical?0:1];
		// Edge is of unknown color check if it is 
		// not already set in solution vector.
		if (edge == this->NO_COLOR_EDGE) {
				int vectorIndex = this->EDGE_INDEX[Edge::createCode(col, row, vertical)];
				if (this->stack.size() > vectorIndex)
					edge = this->stack[vectorIndex];
				else return true;
		}
		
		// No conflict found, continue.
		if (edge != edgeColor) {
				return true;
		}
		return false;
}
        
unsigned char ***Program::merge() {
		unsigned char ***merged = this->GRAPH;
		for (int i = 0; i < this->bestSolutionSize; i++) {
				Edge *edge = this->EDGE_ARRAY[i];
				merged[edge->row][edge->col][edge->vertical?0:1] = this->bestSolution[i];
		}
		return merged;
}

void Program::run() {
	unsigned char edgeColor = 1;
	long int tempSum = 0;
	while(true) {
			// Back track or terminate
			int size = this->stack.size();
			bool finished = false;
			int tmpS = this->EDGE_ARRAY.size();
			finished = (size == tmpS);
			if (edgeColor > this->MAX_VALUE || this->bestScore < tempSum + edgeColor || finished) {
//			if (this->bestScore < tempSum + edgeColor || finished) {
					// Solution found
					if (finished) {
							this->bestSolution = this->stack;
							this->bestSolutionSize = this->stack.size();
							this->bestScore = tempSum;
					}
					if (size > 0) {
							unsigned char pop = this->stack[this->stack.size()-1];
							this->stack.pop_back();
							tempSum -= pop;
							edgeColor = ++pop;
							continue;
					}
					break;
			}
			
			bool valid = false;
			valid = this->validInsert(edgeColor, size);
			
			if (valid) {
					this->stack.push_back(edgeColor);
					tempSum += edgeColor;
					edgeColor = 1;
					continue;
			}
			edgeColor++;
	}
}

void Program::print() {
	cout << "Unsolved problem." << endl;
	Printer::print(this->GRAPH, this->row, this->col);
	cout << "Solved problem. \nPrice: " << this->bestScore << " \n";
	cout << "Solution vector: "; 
	cout << "[ ";
	for (int i = 0; i < this->bestSolutionSize; i++)
		cout << (int)this->bestSolution[i] << " "; 
	cout << "]";
	cout << endl;
	Printer::print(this->merge(), this->row, this->col);
};


