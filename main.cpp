/* 
 * File:   main.cpp
 * Author: Degee
 *
 * Created on 21. říjen 2013, 23:43
 */

#include <cstdlib>
#include "Program.h"

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
	
	//Program program(argv[1]);
	Program program("data2.txt");
	program.print();
	
	program.run();
	
    program.print();
        
	return EXIT_SUCCESS;
}

