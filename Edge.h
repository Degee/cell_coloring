/* 
 * File:   Edge.cpp
 * @Author: Jiří Levý, Lukáš Svoboda
 * 
 * @brief
 * Class represents Edge. It is used for purpose of indexing edges of
 * unknown color.
 * 
 * Created on 22. říjen 2013, 13:42
 */

#include<iostream>
#include<string>

using namespace std;

#ifndef EDGE_H
#define	EDGE_H

class Edge {
public:
	int			col;
	int			row;
	bool		vertical;
				
				Edge(int x, int y, bool vertical);
				Edge(const Edge& orig);
				virtual ~Edge();
				
	//string		toString();
	int			hashCode();
	static int	createCode(int x, int y, bool vertical);
	bool		equals(Edge *obj);
				
private:

};

#endif	/* EDGE_H */

