/* 
 * File:   Printer.h
 * Author: Degee
 *
 * Created on 22. říjen 2013, 13:41
 */

#include<string>
#include<cstring>
#include<iostream>

#ifndef PRINTER_H
#define	PRINTER_H

using namespace std;

class Printer {
public:
				Printer();
				Printer(const Printer& orig);
				virtual ~Printer();

	static void	print(unsigned char *** graph,int x,int y);

private:
	static string eval(unsigned char b);

};

#endif	/* PRINTER_H */

