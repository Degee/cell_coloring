/* 
 * File:   Printer.cpp
 * Author: Degee
 * 
 * Created on 22. říjen 2013, 13:41
 */

#include "Printer.h"


Printer::Printer() {
}

Printer::Printer(const Printer& orig) {
}

Printer::~Printer() {
}

void Printer::print(unsigned char*** graph, int row, int col) {
	for (int y = 0; y < row; y++) {
                        
		string horizontal;
		string vert1;
		string vert2;

		for (int x = 0; x < col; x++) {
				unsigned char hor = graph[y][x][1];
				unsigned char ver = graph[y][x][0];

				if ((horizontal.length() > 0 && horizontal[horizontal.length()-1] != 'o') 
						|| horizontal.length() == 0)
							horizontal.append(" ");

				if (hor != 255) {
						horizontal.append("--"+eval(hor)+"--");
				} else {
					horizontal.append("     ");
				}

				string eval = Printer::eval(ver);
				if (eval != " ")
						vert1.append("|");
				else 
						vert1.append(" ");
				vert1.append("     ");
				vert2.append(eval);
				vert2.append("     ");
		}

		cout << horizontal << endl;
		cout << vert1 << endl;
		cout << vert2 << endl;
		cout << vert1 << endl;
	}
}
        
string Printer::eval(unsigned char b) {
	string val = "";
	if (b == 255)
			val = " ";
	else if (b == 0)
			val = "?";
	else 
			val = b+'0';
	return val;
}

