#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc.exe
CCC=g++.exe
CXX=g++.exe
FC=g77.exe
AS=as.exe

# Macros
CND_PLATFORM=Cygwin_4.x-Windows
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/_ext/234932500/Edge.o \
	${OBJECTDIR}/_ext/234932500/Program.o \
	${OBJECTDIR}/_ext/234932500/Printer.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/cell_coloring.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/cell_coloring.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/cell_coloring ${OBJECTFILES} ${LDLIBSOPTIONS} 

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/main.o main.cpp

${OBJECTDIR}/_ext/234932500/Edge.o: /cygdrive/C/Users/Degee/Documents/NetBeansProjects/cell_coloring/Edge.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/234932500
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/234932500/Edge.o /cygdrive/C/Users/Degee/Documents/NetBeansProjects/cell_coloring/Edge.cpp

${OBJECTDIR}/_ext/234932500/Program.o: /cygdrive/C/Users/Degee/Documents/NetBeansProjects/cell_coloring/Program.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/234932500
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/234932500/Program.o /cygdrive/C/Users/Degee/Documents/NetBeansProjects/cell_coloring/Program.cpp

${OBJECTDIR}/_ext/234932500/Printer.o: /cygdrive/C/Users/Degee/Documents/NetBeansProjects/cell_coloring/Printer.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/234932500
	${RM} $@.d
	$(COMPILE.cc) -g -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/234932500/Printer.o /cygdrive/C/Users/Degee/Documents/NetBeansProjects/cell_coloring/Printer.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/cell_coloring.exe

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
